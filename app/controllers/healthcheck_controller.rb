# frozen_string_literal: true

class HealthcheckController < ApplicationController
  def index
    render json: { message: 'cool beans' }, status: :ok
  end
end