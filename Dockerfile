FROM ruby:2.7.0-alpine

RUN apk update && apk add --no-cache nodejs build-base libxml2-dev libxslt-dev curl git yarn

RUN mkdir /app
WORKDIR /app

COPY Gemfile* package.json ./
RUN gem install bundler && bundle install -j 20 && yarn install

COPY . ./

RUN bundle exec rails assets:precompile

EXPOSE 3000
CMD puma -C config/puma.rb