module SecretManagerService
  def self.load_to_env
    if Rails.env.production? || Rails.env.staging?
      File.readlines('.env').each do |line|
        setting = line.split('=')
        ENV[setting[0].strip] = setting[1].strip
      end

      client = Aws::SecretsManager::Client.new

      secret_value_response = client.get_secret_value(secret_id: "#{ENV['RAILS_ENV']}/odin")
      secret_value = ''
      if secret_value_response.secret_string
        secret_value = secret_value_response.secret_string
      else
        secret_value = Base64.decode64(secret_value_response.secret_binary)
      end

      JSON.parse(secret_value).each_pair do |k, v|
        ENV["#{k}".underscore.upcase] = v
      end
    end
  end
end